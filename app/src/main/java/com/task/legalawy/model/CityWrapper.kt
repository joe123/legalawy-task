package com.task.legalawy.model

import com.task.legalawy.model.dto.response.ACity
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CityWrapper {
    var cities: List<ACity>? = null
    var citieNames: ArrayList<String>? = null
    var citiesMap =hashMapOf<String, ACity?>()
}