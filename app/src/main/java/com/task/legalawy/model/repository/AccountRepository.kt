package com.task.legalawy.model.repository

import androidx.fragment.app.Fragment

interface AccountRepository {
     fun signInWithFacebook(context: Fragment?): Boolean
     fun signInWithGoogle(context: Fragment?): Boolean

}