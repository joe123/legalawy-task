package com.task.legalawy.model.repository

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import org.json.JSONException
import java.util.*


class AccountRepositoryImpl : BaseRepository(), AccountRepository {

    override fun signInWithFacebook(context: Fragment?): Boolean {
        facebookLogin(context)
        return true
    }

    fun facebookLogin(context: Fragment?) {
        val callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .logIn(context!!.childFragmentManager.fragments.get(0), Arrays.asList("")) //public_profile
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    Log.i("MainActivity", "@@@onSuccess")
                    val request = GraphRequest.newMeRequest(
                        loginResult.accessToken
                    ) { `object`, response ->
                        Log.i("MainActivity", "@@@response: $response")
                        try {
                            val name = `object`.getString("name")
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,name,email,gender, birthday")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    Log.i("MainActivity", "@@@onCancel")
                }

                override fun onError(error: FacebookException) {
                    Log.i("MainActivity", "@@@onError: " + error.message)
                }
            })
    }

    override fun signInWithGoogle(context: Fragment?): Boolean {
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        var mGoogleSignInClient = GoogleSignIn.getClient(context!!.requireActivity(), gso)
        val signInIntent = mGoogleSignInClient.signInIntent
        context.childFragmentManager.fragments.get(0)
            .startActivityForResult(signInIntent, Companion.RC_SIGN_IN)

        return true
    }

    companion object {
        public const val RC_SIGN_IN: Int = 100
    }

}