package com.task.legalawy.model.repository

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.task.legalawy.AppClass
import com.task.legalawy.model.CityWrapper
import com.task.legalawy.model.dto.response.ACity
import com.task.legalawy.utils.AppPrefs
import com.task.legalawy.utils.FileSystemHelper
import com.task.legalawy.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.reflect.Type


class CityRepositoryImpl : BaseRepository(), CityRepository {
    private val mutableLiveData =
        SingleLiveEvent<CityWrapper>()

    override fun getAllCities(): CityWrapper {
        var prefs = AppPrefs.get()
//        var cityWrapper = prefs?.getCities()
//        if (cityWrapper == null) {
        var cityWrapper = CityWrapper()
        var citiesString =
            FileSystemHelper.readFileFromAssests(AppClass.instance!!, "cities.json");
        val gson = Gson()
        val listType: Type = object : TypeToken<List<ACity?>?>() {}.type
        val allCities: List<ACity> =
            gson.fromJson<List<ACity>>(citiesString, listType)
        cityWrapper.cities = allCities
        cityWrapper.citieNames = arrayListOf<String>()
        for ((index, value) in allCities.withIndex()) {
            cityWrapper.citieNames!!.add(value.city)
            cityWrapper.citiesMap.put(value.city, value)
        }

//        }
        return cityWrapper
    }
}