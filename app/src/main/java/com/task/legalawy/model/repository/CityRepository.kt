package com.task.legalawy.model.repository

import androidx.lifecycle.LiveData
import com.task.legalawy.model.CityWrapper
import com.task.legalawy.model.dto.response.ACity

interface CityRepository {
    fun getAllCities(): CityWrapper
}