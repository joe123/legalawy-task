package com.task.legalawy.model.dto.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ACity(

    @SerializedName("country") val country: String,
    @SerializedName("name") val city: String,
    @SerializedName("lat") val lat: Double,
    @SerializedName("lng") val lon: Double
) : Serializable