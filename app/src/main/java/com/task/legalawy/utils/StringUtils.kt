package com.task.legalawy.utils

object StringUtils {
    fun capitalize(text: String): String {
        return text.toUpperCase()
    }
}