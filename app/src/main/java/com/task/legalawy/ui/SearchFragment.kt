package com.task.legalawy.ui

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.legalawy.BaseFragment
import com.task.legalawy.R
import com.task.legalawy.model.CityWrapper
import com.task.legalawy.model.dto.response.ACity
import com.task.legalawy.ui.adapter.CityAdapter
import com.task.legalawy.viewmodel.CityViewModel
import kotlinx.android.synthetic.main.fragment_search2.*
import org.koin.androidx.viewmodel.ext.android.viewModel

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : BaseFragment() {
    private lateinit var viewAdapter: CityAdapter
    val cityViewModel: CityViewModel by viewModel()
    lateinit var cities: List<ACity>
    lateinit var filteredCities: List<ACity>
    lateinit var cityWrapper: CityWrapper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_search2
    }

    override fun doOnViewCreated(view: View, savedInstanceState: Bundle?) {
        super.doOnViewCreated(view, savedInstanceState)
        observeProgressBar()
        getAllCities()
        setListeners()
    }

    private fun setListeners(): Unit {
        imageView.setOnClickListener({
            handleSearch()
        })
        et_search.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                handleSearch()
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun handleSearch(): Unit {
        if (cityWrapper == null) {
            Toast.makeText(
                requireActivity(),
                getString(R.string.loading_data),
                Toast.LENGTH_LONG
            ).show()
            return@handleSearch Unit
        }
        var query = et_search.text.toString()
        if (query.isEmpty()) {
            Toast.makeText(
                requireActivity(),
                getString(R.string.enter_query),
                Toast.LENGTH_LONG
            ).show()
        } else {
            filteredCities = cities.filter { it.city.toLowerCase().contains(query.toLowerCase()) }
            refreshAdapter()
        }
    }

    private fun refreshAdapter() {
        viewAdapter = CityAdapter(
            filteredCities,
            requireActivity()
        )
        var linearLayoutManager = LinearLayoutManager(activity)
        rv_cities.layoutManager = linearLayoutManager
        rv_cities.adapter = viewAdapter
    }

    private fun getAllCities() {
        cityViewModel.getCities().observe(viewLifecycleOwner, Observer {
            cityWrapper = it
            cities = it.cities!!
            Toast.makeText(requireActivity(), getString(R.string.ready), Toast.LENGTH_LONG).show()
        })
    }

    private fun observeProgressBar() {
        cityViewModel.progressVisibility.observe(viewLifecycleOwner, Observer {
            toggleProgressBarState(it)
        })
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            SearchFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}