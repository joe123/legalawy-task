package com.task.legalawy.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.task.legalawy.R
import com.task.legalawy.model.dto.response.ACity

class CityAdapter(
    private val cities: List<ACity>,
    var context: Context
) : RecyclerView.Adapter<CityAdapter.NewsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.viewholder_city_details, parent, false)
        return NewsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val aCity = cities[position]
        holder.tv_city.text = aCity.city
        holder.tv_country.text = aCity.country
        holder.tv_lat.text = aCity.lat.toString()
        holder.tv_long.text = aCity.lon.toString()

    }

    override fun getItemCount(): Int {
        return cities.size
    }


    inner class NewsViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        var tv_city: TextView
        var tv_country: TextView
        var tv_lat: TextView
        var tv_long: TextView

        init {
            tv_city = view.findViewById(R.id.tv_city)
            tv_country = view.findViewById(R.id.tv_country)
            tv_lat = view.findViewById(R.id.tv_lat)
            tv_long = view.findViewById(R.id.tv_long)
        }

    }
}