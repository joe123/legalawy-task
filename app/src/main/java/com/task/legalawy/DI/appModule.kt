package com.task.legalawy.DI

import com.task.legalawy.model.repository.*
import com.task.legalawy.viewmodel.AccountViewModel
import com.task.legalawy.viewmodel.CityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

@JvmField
val appModule = module {

    factory<AccountRepository> { AccountRepositoryImpl() }
    viewModel { AccountViewModel(get()) }
    factory<CityRepository> { CityRepositoryImpl() }
    viewModel { CityViewModel(get()) }
}


@JvmField
val testModule = module {

    factory<AccountRepository> { AccountRepositoryDummy() }
    viewModel { AccountViewModel(get()) }

    factory<CityRepository> { CityRepositoryDummy() }
    viewModel { CityViewModel(get()) }
}