package com.task.legalawy.viewmodel

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.task.legalawy.model.repository.AccountRepository
import com.task.legalawy.ui.MainActivity
import com.task.legalawy.utils.SingleLiveEvent


class AccountViewModel(accountRepository: AccountRepository) : BaseViewModel() {
    public val loginSuccess =
        SingleLiveEvent<Boolean>()
    private var accountRepositoryImpl: AccountRepository? = null


    init {
        accountRepositoryImpl = accountRepository
    }

    fun signInFaceBook(context: Context) {
        accountRepositoryImpl!!.signInWithFacebook(getVisibleFragment(context as MainActivity))
    }

    fun signIn() {
        loginSuccess.value = true
    }

    fun signInGoogle(context: Context) {
        accountRepositoryImpl!!.signInWithGoogle(
            getVisibleFragment(context as MainActivity)
        )
    }

    fun getVisibleFragment(mainActivity: MainActivity): Fragment? {
        val fragmentManager: FragmentManager = mainActivity.getSupportFragmentManager()
        val fragments: List<Fragment> = fragmentManager.getFragments()
        if (fragments != null) {
            for (fragment in fragments) {
                if (fragment != null && fragment.isVisible()) return fragment
            }
        }
        return null
    }
}

