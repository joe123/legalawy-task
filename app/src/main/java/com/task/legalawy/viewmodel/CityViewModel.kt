package com.task.legalawy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.task.legalawy.model.CityWrapper
import com.task.legalawy.model.repository.CityRepository
import com.task.legalawy.utils.SingleLiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CityViewModel(cityRepository: CityRepository) : BaseViewModel() {
    private var cityRepository: CityRepository? = null
    private val mutableLiveData =
        SingleLiveEvent<CityWrapper>()

    val dataSource =
        MutableLiveData<List<String>>(emptyList<String>())


    init {
        this.cityRepository = cityRepository
    }

    fun getCities(): LiveData<CityWrapper> {
        coroutineScope.launch(Dispatchers.Main) {
            progressVisibility.value = true
            var cities = fetchCities()
            mutableLiveData.value = cities
            progressVisibility.value = false
//            dataSource.value = cities.citiesName
        }
        return mutableLiveData
    }


    suspend fun fetchCities(): CityWrapper {
        return withContext(Dispatchers.IO)
        {
            cityRepository!!.getAllCities()
        }!!
    }

}